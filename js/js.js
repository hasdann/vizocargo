$(document).foundation();

$(document).ready(function(){
  $('.banners-slider').bxSlider({
  	minSlider: 5,
  	maxSlides: 5,
    slideWidth: 250,
  	pager: false
  });

  $('.hp__slider').bxSlider({
  	pager: false,
  	pagerCustom: '.bx-pager'
  });
});

new WOW().init();

window.onscroll = function() {fixedMenu()};

  function fixedMenu() {
     if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("sticky-head").style.top = 0;
    document.getElementById("subnav-sticky").style.top = 120 +"px";
  } else {
    document.getElementById("sticky-head").style.top = 80 + "px";
    document.getElementById("subnav-sticky").style.top = 200 + "px";
  }
    }